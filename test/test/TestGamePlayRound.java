package test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import game.Dice;
import game.DiceValue;
import game.Game;
import game.Player;

public class TestGamePlayRound 
{
	private int mockBet_;
	
	private Player    player1_;
	private Game      sut_;
	private Dice      dice_;
	private DiceValue diceValue_;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
		player1_   = new Player("test1", 100);
		dice_      = new Dice();
		diceValue_ = dice_.getValue();
		mockBet_   = 5;
		sut_       = new Game(dice_, dice_, dice_);
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testPlayRoundIncorrectBalanceReturned() 
	{
		// This test will fail once the code has been fixed.
		// arrange
		// to show that the calculation is incorrect, expected amount should be 110 instead of the
		// correct amount of 115
		int expected = player1_.getBalance() + 10;
		
		// execute
		sut_.playRound(player1_, diceValue_, mockBet_);
		int actual = player1_.getBalance();
		
		// assert
		assertEquals(expected, actual);
	}
}
