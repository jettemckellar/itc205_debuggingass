package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import game.Dice;
import game.DiceValue;

public class TestDiceValue 
{
	private DiceValue club_;
	private DiceValue crown_;
	private DiceValue anchor_;
	private DiceValue heart_;
	private DiceValue diamond_;
	private DiceValue spade_;
	private Dice      sut_;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
		club_    = DiceValue.CLUB;
		crown_   = DiceValue.CROWN;
		anchor_  = DiceValue.ANCHOR;
		heart_   = DiceValue.HEART;
		diamond_ = DiceValue.DIAMOND;
		spade_   = DiceValue.SPADE;
		sut_     = new Dice();
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testToStringValueReturned() 
	{
		System.out.println("Test toString method within Dice which calls DiceValue toString method");
		System.out.println("Run: " + sut_.toString());
		System.out.println("Test toString() method completed\n");		
	}
	
	@Test
	public void testGetRandom() 
	{
		System.out.println("Test DiceValue.getRandom():");
		int num = 20;
		for (int i = 0; i < num; i++) {
			System.out.println("Run " + i + ": " + DiceValue.getRandom());
		}
		System.out.println("Test getRandom() method completed");
	}

}
