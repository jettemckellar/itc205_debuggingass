package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import game.Dice;
import game.DiceValue;
import game.Game;

public class TestGame 
{
	private Dice            dice1_;
	private Dice            dice2_;
	private Dice            dice3_;
	private List<Dice>      dice_;
	private List<DiceValue> values_;
	private Game            sut_;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testGameConstructorUsingGetDiceValuesMethodTest1() 
	{
		// Checking that the array constructed in Game constructor remains the same when called in the 
		// getDiceValues() method
		// arrange

		dice1_ = new Dice();
		dice2_ = new Dice();
		dice3_ = new Dice();
		
		List<Dice> diceList = new ArrayList<>();
		diceList.add(dice1_);
		diceList.add(dice2_);
		diceList.add(dice3_);
		
		List<DiceValue> dv = new ArrayList<>();
		
				
		// execute
		sut_ = new Game(dice1_, dice2_, dice3_);
		
		
		System.out.println("Values passed to the Game getDiceValues method Test1");
		values_ = sut_.getDiceValues();
		for (Dice d: diceList) {
			dv.add(d.getValue());
		}

		for (int i = 0; i < dv.size(); i++) {
			System.out.print(dv.get(i) + " ");
		}
		System.out.println();
	}
	
	@Test
	public void testGameConstructorUsingGetDiceValuesMethodTest2() 
	{
		// Checking that the array constructed in Game constructor remains the same when called in the 
		// getDiceValues() method
		// arrange

		dice1_ = new Dice();
		dice2_ = new Dice();
		dice3_ = new Dice();
		
		List<Dice> diceList = new ArrayList<>();
		diceList.add(dice1_);
		diceList.add(dice2_);
		diceList.add(dice3_);
		
		List<DiceValue> dv = new ArrayList<>();
		
				
		// execute
		sut_ = new Game(dice1_, dice2_, dice3_);
		
		
		System.out.println("Values passed to the Game getDiceValues method Test 2");
		values_ = sut_.getDiceValues();
		for (Dice d: diceList) {
			dv.add(d.getValue());
		}

		for (int i = 0; i < dv.size(); i++) {
			System.out.print(dv.get(i) + " ");
		}
		System.out.println();
	}	
	
	@Test
	public void testGameConstructorUsingGetDiceValuesMethodTest3() 
	{
		// Checking that the array constructed in Game constructor remains the same when called in the 
		// getDiceValues() method
		// arrange

		dice1_ = new Dice();
		dice2_ = new Dice();
		dice3_ = new Dice();
		
		List<Dice> diceList = new ArrayList<>();
		diceList.add(dice1_);
		diceList.add(dice2_);
		diceList.add(dice3_);
		
		List<DiceValue> dv = new ArrayList<>();
		
				
		// execute
		sut_ = new Game(dice1_, dice2_, dice3_);
		
		
		System.out.println("Values passed to the Game getDiceValues method Test 3");
		values_ = sut_.getDiceValues();
		for (Dice d: diceList) {
			dv.add(d.getValue());
		}

		for (int i = 0; i < dv.size(); i++) {
			System.out.print(dv.get(i) + " ");
		}
		System.out.println();
	}	
	
	@Test
	public void testGameConstructorUsingGetDiceValuesMethodTest4() 
	{
		// Checking that the array constructed in Game constructor remains the same when called in the 
		// getDiceValues() method
		// arrange

		dice1_ = new Dice();
		dice2_ = new Dice();
		dice3_ = new Dice();
		
		List<Dice> diceList = new ArrayList<>();
		diceList.add(dice1_);
		diceList.add(dice2_);
		diceList.add(dice3_);
		
		List<DiceValue> dv = new ArrayList<>();
		
				
		// execute
		sut_ = new Game(dice1_, dice2_, dice3_);
		
		
		System.out.println("Values passed to the Game getDiceValues method Test 4");
		values_ = sut_.getDiceValues();
		for (Dice d: diceList) {
			dv.add(d.getValue());
		}

		for (int i = 0; i < dv.size(); i++) {
			System.out.print(dv.get(i) + " ");
		}
		System.out.println();
	}	
	
	@Test
	public void testGameConstructorUsingGetDiceValuesMethodTest5() 
	{
		// Checking that the array constructed in Game constructor remains the same when called in the 
		// getDiceValues() method
		// arrange

		dice1_ = new Dice();
		dice2_ = new Dice();
		dice3_ = new Dice();
		
		List<Dice> diceList = new ArrayList<>();
		diceList.add(dice1_);
		diceList.add(dice2_);
		diceList.add(dice3_);
		
		List<DiceValue> dv = new ArrayList<>();
		
				
		// execute
		sut_ = new Game(dice1_, dice2_, dice3_);
		
		
		System.out.println("Values passed to the Game getDiceValues method Test 5");
		values_ = sut_.getDiceValues();
		for (Dice d: diceList) {
			dv.add(d.getValue());
		}

		for (int i = 0; i < dv.size(); i++) {
			System.out.print(dv.get(i) + " ");
		}
	}	
}
