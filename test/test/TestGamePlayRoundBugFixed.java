package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import game.Dice;
import game.DiceValue;
import game.Game;
import game.Player;

public class TestGamePlayRoundBugFixed 
{
    private int       bet_;	
	private Player    player1_;
	private Game      sut_;
	private Dice      dice_;
	private DiceValue diceValue_;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
		
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testCorrectBalanceReturned() 
	{
		// arrange
		player1_   = new Player("test1", 100);
		dice_      = new Dice();
		diceValue_ = dice_.getValue();
		bet_       = 5;
		sut_       = new Game(dice_, dice_, dice_);
        int expected = player1_.getBalance() + 15;
		
		// execute
		sut_.playRound(player1_, diceValue_, bet_);
		int actual = player1_.getBalance();
		
		// assert
		assertEquals(expected, actual);
	}
}
