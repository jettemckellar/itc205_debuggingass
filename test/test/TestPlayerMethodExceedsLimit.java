package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import game.Player;

public class TestPlayerMethodExceedsLimit 
{
	private Player sut_;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
		int balance = 5;
		int limit   = 0;
		String name = "Fred";
		sut_        = new Player(name, balance);
		sut_.setLimit(limit);
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testBalanceExceedsLimit() 
	{
		// arrange
		boolean expected = false;
		int bet          = 5;
		int expectedBal  = 0;
		sut_.takeBet(bet);
				
		// execute
		boolean actual = sut_.balanceExceedsLimit();
		int actualBal  = sut_.getBalance(); 
				
		// asserts
		assertEquals(expected,actual); 
		assertEquals(expectedBal, actualBal);
	}
}
