package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import game.Player;

public class TestPlayerMethodBalanceExceedsLimitBy 
{
	private Player sut_;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{	
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testBalanceExceedsLimitBy() 
	{
		// arrange
		int balance = 5;
		int limit   = 0;
		String name = "Fred";
		sut_        = new Player(name, balance);
		
		sut_.setLimit(limit);
		
		boolean expectedRun1 = true;
		boolean expectedRun2 = false;
		int bet              = 5;
		int expectedBal      = 0;
		
		// execute
		boolean actualRun1 = sut_.balanceExceedsLimitBy(bet);
		sut_.takeBet(bet);
		int actualBal  = sut_.getBalance(); 
		boolean actualRun2 = sut_.balanceExceedsLimitBy(bet);
		
		// asserts
		assertEquals(expectedRun1, actualRun1); 
		assertEquals(expectedBal, actualBal);
		assertEquals(expectedRun2, actualRun2);
	}
}
